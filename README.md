# **NproWebman** 项目自用二维码生成SDK。
## 安装
#### 执行命令安装
```
"require": {
    "npro/qrcode-webman":"^1.0|@dev"
},
```
```
composer require npro/qrcode-webman
```


## 使用
#### 添加配置文件
```
return [
    'cache_dir' => 'uploads'.DS.'qrcode',           //缓存地址
    'background'=> 'static/image/icon_cover.png'    //背景图
];
```

#### 使用方法
```
use npro/qrcode/QRcode;
$code = new QRcode();
$code_path =  $code->png($register_url)         //生成二维码
    ->logo('static/image/avatar-m.jpg')         //生成logo二维码
    ->background(180,500)                       //给二维码加上背景
    ->text($role,20,['center',740],'#ff4351')   //添加文字水印
    ->text($nick_name,20,['center',780],'#000000')
    ->getPath();                                //获取二维码生成的地址

```